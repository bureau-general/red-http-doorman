Gem::Specification.new do |s|
  s.name        = 'red-http-gatekeeper'
  s.version     = '0.2020.03.07'
  s.licenses    = []
  s.summary     = "RED's HTTP Gatekeeper"
  s.authors     = ["Luc Bournaud"]
  s.email       = 'bournaud@et.esiea.fr'
  s.files       = ["lib/red-http-gatekeeper.rb"]
  s.add_runtime_dependency 'base64'
  s.add_runtime_dependency 'etc'
  s.add_runtime_dependency 'gssapi'
  s.add_runtime_dependency 'mysql2'
  s.add_runtime_dependency 'syslog'
end

