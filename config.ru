require 'rack/reloader'
require 'red-http-gatekeeper'

### Configuration ###
# The ActiveDirectory realm
$REALM = 'RED-ASSO.FR'

# The SQL connection
# TODO Set default schema to 'redasso_main'
$sql = Mysql2::Client::new()

use Rack::Reloader
run ->(env) { APP.call(env) }
