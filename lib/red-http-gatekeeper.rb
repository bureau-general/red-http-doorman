# RED HTTP gatekeeper
# Copyright (c) 2020 RED ESIEA Laval
# Author: Luc BOURNAUD
#
# MariaDB bootstrap:
# CREATE SCHEMA red_api_gateway;
# CREATE TABLE red_api_gateway.allow (
# 	hga_principal   VARCHAR(64),
# 	hga_principal   VARCHAR(64),
# 	hga_method VARCHAR(16) NOT NULL,
# 	hga_path   VARCHAR(256) NOT NULL
# );
#
# To grant access to hga_path using hga_method HTTP method :
# 	- NULL to allow anonymous access
# 	- '' (empty string) to allow universal access to authenticated users
# 	- '<username>' to allow to grant access to a single user

# frozen_string_literal: true
require 'base64'
require 'etc'
require 'gssapi'
require 'mysql2'
require 'syslog/logger'

# Open syslog
Syslog.open('red-http-gatekeeper',0,Syslog::LOG_AUTHPRIV)

# Check if an ActiveDirectory user have access to a resource
def check_access(user,host,method,path)
	sql           = $sql
	hga_principal = sql.escape(user)
	hga_method    = sql.escape(method)
	hga_path      = sql.escape(path)
	hga_host   = sql.escape(host)
	
	sql.query("SELECT hga_principal FROM redasso_main.http_gatekeeper_allow WHERE hga_host = '#{hga_host}' AND hga_method = '#{hga_method}' AND hga_path = '#{hga_path}';", :as => :array).each do |row|
		if    row[0] == nil then # Check for anonymous access
			Syslog.log(Syslog::LOG_INFO,"#{user}:#{hga_method} #{hga_host}#{hga_path} granted (<anonymous>)")
			return true;
		elsif row[0] == ''  then # Check any authenticated access
			Syslog.log(Syslog::LOG_INFO,"#{user}:#{hga_method} #{hga_host}#{hga_path} granted (<any>)")
			return true;
		elsif (row[0] == user) or (Etc.getgrnam(row[0]).include? user) then # Check for user or group match
			Syslog.log(Syslog::LOG_INFO,"#{user}:#{hga_method} #{hga_host}#{hga_path} granted (#{row[0]})")
			return true
		end
	end
	
	# Authentication failed
	Syslog.log(Syslog::LOG_INFO,"#{user}:#{hga_method} #{hga_host}#{hga_path} rejected}")
	return false
end

# Check if an anonymous user have access to a resource
def check_access_anonymous(host,method,path)
	allow      = false
	sql        = $sql
	hga_method = sql.escape(method)
	hga_path   = sql.escape(path)
	hga_host   = sql.escape(host)
	
	sql.query("SELECT 1 FROM redasso_main.http_gatekeeper_allow WHERE hga_principal IS NULL AND hga_host = '#{hga_host}' AND hga_method = '#{hga_method}' AND hga_path = '#{hga_path}' LIMIT 1;").each do |row|
		allow = true
	end
	
	Syslog.log(Syslog::LOG_INFO,"<anonymous>:#{hga_method} #{hga_host}#{hga_path} #{allow ? 'granted' : 'rejected'}")
	return allow
end

handler = ->(env) {
	# Parse query
	Rack::Utils::parse_nested_query(env['QUERY_STRING'])
	host   = env['HTTP_HOST']
	method = env['REQUEST_METHOD']
	path   = env['PATH_INFO']
	
	# Check or SPNEGO
	authorization_token = env['HTTP_AUTHORIZATION']
	reply_headers = {}
	
	if authorization_token then
		# Perform SPNEGO
		authorization_token = Base64.decode64(authorization_token[10..-1]) # Strip 'Negociate ' and decode the token
		
		# Perform dark GSSAPI magic
		gss = GSSAPI::Simple::new($REALM,'http/'+host)
		gss.acquire_credentials()
		begin
			reply_headers = {
				'WWW-Authenticate' => 'Negociate '+Base64.encode64(gss.accept_context(authorization_token)),
				'X-RED-Gatekeeper-Principal' => gss.display_name,
			}
		rescue GssApiError # If auth goes wrong
			return [401, {'WWW-Authenticate' => 'Negociate'}, ['You must login']]
		end
		
		# Check if the user can login
		if check_access(host,gss.display_name,method,path) then
			return [200, reply_headers, ['Access granted']]
		else
			return [403, reply_headers, ['Access rejected']]
		end
	else
		# No SPNEGO - Check if anonymous access is allowed
		if check_access_anonymous(host,method,path) then
			return [200, reply_headers, ['Access granted']]
		else
			return [401, {'WWW-Authenticate' => 'Negociate'}, ['You must login']]
		end
	end
	return [500, {}, ['Uh. How did I get there ?']]
}

APP = handler
